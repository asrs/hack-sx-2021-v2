# TODO LIST

- create tar.gz for ex01 & ex02
- copie from root tar.gz to user when adduser.script
- during grade script before exiting, commit container ex01 & ex02 at user space & create archive with new name
- during grading process copy containers archive from user
- decompress archive, launch container & test it


# installation

install git and git clone this project a **/root/**
then launch the **./setup.sh** from the cloned directory.

# how to use it

there is script added to your path to manipulate ansible project :

- adduser.script: it will add an user to the hackathon machine
- check.script: it will check if time is passed or not for an user
- gradeuser.script: it will grade the user on the 3 exercise

Those binaries launch ansible project on localhost to instantiate action in a secure way

just add an user with the following command :
```
adduser.script toto toto123
```

Now the user will have 24hour to do the hackathon.
When it login this will create a file in /tmp/user_login.time that will be check by a shadow running process.

The user will have his session close & grade after 20 minute

You will find pid of shadow jobs at the following path: **/root/jobs/**

You will find the final reports at: **/root/user_reports**
